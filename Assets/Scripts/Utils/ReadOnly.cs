﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class ReadOnlyAttribute : PropertyAttribute {

    public readonly bool duringPlayModeOnly;

    public ReadOnlyAttribute(bool duringPlayModeOnly = false) {
        this.duringPlayModeOnly = duringPlayModeOnly;
    }

}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer {
    public override float GetPropertyHeight(SerializedProperty property,
                                            GUIContent label) {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position,
                               SerializedProperty property,
                               GUIContent label) {
        var inPlay = Application.isPlaying;
        GUI.enabled = (attribute as ReadOnlyAttribute).duringPlayModeOnly && !inPlay;
        EditorGUI.PropertyField(position, property, label, true);
        GUI.enabled = true;
    }
}
#endif
