﻿using UnityEngine;

public static class Vector2Ext {

    public static Vector2 Reflect(Vector2 origin, Vector2 direction, Vector2 point) {

        // From https://stackoverflow.com/a/3312306/2115633
        // pl = p + (d * <p - p1, d> / <d, d>)
        // p2 = 2 * pl - p1

        var projection = origin + (direction * Vector2.Dot(point - origin, direction) / Vector2.Dot(direction, direction));
        return point + 2 * (projection - point);

    }

    public static bool IsAngleOrdered(Vector2 origin, Vector2 a, Vector2 b) {

        //Find Angles
        var angleA = Mathf.Atan2(a.y - origin.y, a.x - origin.x) + Mathf.PI;
        var angleB = Mathf.Atan2(b.y - origin.y, b.x - origin.x) + Mathf.PI;

        //Find min and max Angles
        var minAngle = Mathf.Min(angleA, angleB);
        var maxAngle = Mathf.Max(angleA, angleB);

        //Boolean checks
        var isBackwards = (minAngle + (Mathf.PI * 2 - maxAngle)) < Mathf.PI;
        var angleBIsSmaller = angleB < angleA;

        // Return the correct vector
        return isBackwards ? !angleBIsSmaller : angleBIsSmaller;
    }

}
