﻿using UnityEngine;

namespace InfiniteVoronoi {

    public class IVHalfEdge {
        
        public IVCell Cell { get; private set; }
        public IVHalfEdge Twin { get; private set; }
        public IVHalfEdge Next { get; private set; }
        public Vector2 Origin { get; private set; }

        public object Data { get; set; }

        public IVHalfEdge(IVCell cell) {
            this.Cell = cell;
        }

        public void SetTwin(IVHalfEdge twin) {
            Twin = twin;
        }

        public void SetNext(IVHalfEdge next) {
            Next = next;
        }

        public void SetOrigin(Vector2 origin) {
            Origin = origin;
        }

    }

}
