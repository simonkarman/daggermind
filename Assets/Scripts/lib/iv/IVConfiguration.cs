﻿using System;
using UnityEngine;

namespace InfiniteVoronoi {

    [System.Serializable]
    public class IVConfiguration {
        
        #region Editable
        private bool _editable = true;
        public void MarkAsEditable() {
            _editable = true;
        }

        public void MarkAsNonEditable() {
            _editable = false;
        }

        private void AssertEditable(string name) {
            if (!_editable)
                throw new InvalidOperationException(name + " can not be changed since the Configuration is not editable.");
        }

        public void ValidateAll() {
            Seed = _seed;
            CellsPerChunkRange = _cellsPerChunkRange;
            LloydIterations = _lloydIterations;
            Epsilon = _epsilon;
        }
        #endregion

        #region Values
        [SerializeField]
        private int _seed = 0;
        public int Seed {
            get {
                return _seed;
            }
            set {
                AssertEditable("Seed");
                _seed = value;
            }
        }

        [SerializeField]
        private Vector2Int _cellsPerChunkRange = new Vector2Int(8, 16);
        public Vector2Int CellsPerChunkRange {
            get {
                return _cellsPerChunkRange;
            }
            set {
                AssertEditable("CellsPerChunkRange");
                _cellsPerChunkRange = new Vector2Int(
                    Mathf.Max(value.x, 8),
                    Mathf.Max(value.y, 8)
                );
            }
        }

        [SerializeField]
        private int _lloydIterations = 1;
        public int LloydIterations {
            get {
                return _lloydIterations;
            }
            set {
                AssertEditable("LloydIterations");
                _lloydIterations = Mathf.Clamp(value, 0, 5);
            }
        }

        [SerializeField]
        private float _epsilon = 0.0025f;
        public float Epsilon {
            get {
                return _epsilon;
            }
            set {
                AssertEditable("Epsilon");
                _epsilon = value;
            }
        }
        #endregion
        
    }

}
