﻿using System;
using UnityEngine;

namespace InfiniteVoronoi {

    public class IVCell {

        public int Id { get; private set; }
        public IVChunk Chunk { get; private set; }
        public Vector2 Origin { get; private set; }
        public IVHalfEdge BoundaryEdge { get; private set; }
        public int EdgeCount { get; private set; }

        public object Data { get; set; }

        public IVCell(int id, IVChunk chunk, Vector2 origin, int edgeCount) {
            Id = id;
            Chunk = chunk;
            Origin = origin;
            BoundaryEdge = new IVHalfEdge(this);
            EdgeCount = edgeCount;
        }

        public bool Contains(Vector2 location) {

            // Implementation from Stack Overflow
            // https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon

            var edge = BoundaryEdge;
            bool inside = false;
            do {

                if (
                    ((edge.Next.Origin.y > location.y) != (edge.Origin.y > location.y)) &&
                    (location.x < (edge.Origin.x - edge.Next.Origin.x) * (location.y - edge.Next.Origin.y) / (edge.Origin.y - edge.Next.Origin.y) + edge.Next.Origin.x)
                    ) {
                    inside = !inside;
                }

                edge = edge.Next;

            } while (edge != BoundaryEdge);
            return inside;

        }
    }

}
