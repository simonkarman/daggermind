﻿using UnityEngine;

namespace InfiniteVoronoi {

	public class IVPlane : ConstructableConcurrentDictionary<Vector2Int, IVChunk> {

		public readonly IVConfiguration Configuration;

		public IVPlane(IVConfiguration configuration) : base(ChunkInstantiator) {
			Configuration = configuration;
		}

		private static IVChunk ChunkInstantiator(ConstructableConcurrentDictionary<Vector2Int, IVChunk> creator, Vector2Int key) {
			return new IVChunk(creator as IVPlane, key);
		}

		public IVCell GetCellAt(Vector2 location) {

			var chunkX = Mathf.FloorToInt(location.x);
			var chunkY = Mathf.FloorToInt(location.y);

			for (int x = 0; x < 2; x++) {
				for (int y = 0; y < 2; y++) {

					var diffX = x;
					if (x == 1 && location.x - Mathf.Floor(location.x) < 0.5f) {
						diffX = -1;
					}
					var diffY = y;
					if (y == 1 && location.y - Mathf.Floor(location.y) < 0.5f) {
						diffY = -1;
					}

					var chunk = this.Get(new Vector2Int(chunkX + diffX, chunkY + diffY));
					if (chunk == null)
						continue;

					var cell = chunk.GetCellAt(location);
					if (cell == null)
						continue;

					return cell;
				}
			}

			return null;

		}
	}

}