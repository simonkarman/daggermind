﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace InfiniteVoronoi {

    public class IVChunk : Constructable {

        // Read only
        public readonly IVPlane Plane;
        public readonly Vector2Int Location;

        // The list of cells that have their origin in this chunk
        public List<IVCell> Cells { get; private set; } = new List<IVCell>(); 
        
        public IVChunk(IVPlane plane, Vector2Int location) {
            Plane = plane;
            Location = location;
        }

        public IVCell GetCellAt(Vector2 location) {

            foreach (var cell in Cells) {
                if (!cell.Contains(location))
                    continue;

                return cell;
            }

            return null;
        }

        private static IVHalfEdge TryFindTwinOf(IVHalfEdge halfEdge, IVChunk chunk) {

            // Calculate the expected origin of the Twin Cell
            var expectedTwinCellOrigin = Vector2Ext.Reflect(halfEdge.Origin, halfEdge.Next.Origin - halfEdge.Origin, halfEdge.Cell.Origin);
            var config = chunk.Plane.Configuration;

            // Foreach other cell in the Chunk
            foreach (var twinCell in chunk.Cells) {

                // Skip the cell itself
                if (twinCell == halfEdge.Cell)
                    continue;

                // Skip if not the correct cell
                if ((twinCell.Origin - expectedTwinCellOrigin).sqrMagnitude > config.Epsilon)
                    continue;

                // Loop over all twinHalfEdges in the twinCell, to find the correct one
                var twinHalfEdge = twinCell.BoundaryEdge;
                do {

                    var expectedCellOrigin = Vector2Ext.Reflect(twinHalfEdge.Origin, twinHalfEdge.Next.Origin - twinHalfEdge.Origin, twinHalfEdge.Cell.Origin);

                    // If it is the origin
                    if ((halfEdge.Cell.Origin - expectedCellOrigin).sqrMagnitude < config.Epsilon) {

                        // Return the twinHalfEdge
                        return twinHalfEdge;

                    }

                    twinHalfEdge = twinHalfEdge.Next;

                } while (twinHalfEdge != twinCell.BoundaryEdge);

            }

            return null;

        }

        public override int NumberOfStages => 2;

        protected override void Construct(int stage, Action<float> UpdateProgress) {

            switch (stage) {
                case 0:
                    ConstructStage0(UpdateProgress);
                    return;
                case 1:
                    ConstructStage1(UpdateProgress);
                    return;
                default:
                    break;
            }

        }

        protected override void Deconstruct(int stage, Action<float> UpdateProgress) {
            
        }

        private void ConstructStage0(Action<float> UpdateProgress) {

            // Find the sites that are needed to generated the cells of this chunk
            var config = Plane.Configuration;
            var allSites = new List<Vector2f>();
            var startChunkX = Location.x - 1;
            var startChunkY = Location.y - 1;
            var endChunkX = Location.x + 1;
            var endChunkY = Location.y + 1;
            var sitesHasher = new XXHash(config.Seed);
            for (int chunkX = startChunkX; chunkX <= endChunkX; chunkX++) {
                for (int chunkY = startChunkY; chunkY <= endChunkY; chunkY++) {

                    //var oldSeed = chunkX.GetHashCode() ^ chunkY.GetHashCode() << 2;
                    var seed = (int)sitesHasher.GetHash(chunkX, chunkY);
                    var random = new System.Random(seed);
                    var numberOfSitesDelta = (float)((random.NextDouble() + random.NextDouble()) / 2);
                    var min = config.CellsPerChunkRange.x;
                    var max = config.CellsPerChunkRange.y;
                    var numberOfSites = Mathf.FloorToInt(Mathf.Lerp(min, max, numberOfSitesDelta));

                    for (int i = 0; i < numberOfSites; i++) {
                        var siteX = chunkX + (float)random.NextDouble();
                        var siteY = chunkY + (float)random.NextDouble();

                        allSites.Add(new Vector2f(siteX, siteY));
                    }
                }
            }

            // Calculate the voronoi diagram of the given sites
            var bounds = new Rectf(Location.x - 2, Location.y - 2, 5, 5);
            var voronoi = new csDelaunay.Voronoi(allSites, bounds, config.LloydIterations);

            // Populate the chunk with cells based on the voronoi diagram
            var sites = voronoi.SitesIndexedByLocation;
            foreach (var siteKVP in sites) {
                var site = siteKVP.Value;
                bool innerSite = site.x >= Location.x && site.x < Location.x + 1 && site.y >= Location.y && site.y < Location.y + 1;

                var reorderer = new csDelaunay.EdgeReorderer(site.Edges, typeof(csDelaunay.Vertex));
                var edges = reorderer.Edges;

                if (innerSite) {
                    var cell = new IVCell(Cells.Count, this, new Vector2(site.x, site.y), edges.Count);

                    IVHalfEdge currentHalfEdge = cell.BoundaryEdge;
                    for (int edgeIndex = 0; edgeIndex < edges.Count; edgeIndex++) {
                        var edge = edges[edgeIndex];

                        var pointA = new Vector2(edge.ClippedEnds[csDelaunay.LR.LEFT].x, edge.ClippedEnds[csDelaunay.LR.LEFT].y);
                        var pointB = new Vector2(edge.ClippedEnds[csDelaunay.LR.RIGHT].x, edge.ClippedEnds[csDelaunay.LR.RIGHT].y);

                        var origin = Vector2Ext.IsAngleOrdered(cell.Origin, pointA, pointB) ? pointA : pointB;
                        currentHalfEdge.SetOrigin(new Vector2(origin.x, origin.y));

                        var nextHalfEdge = edgeIndex == edges.Count - 1 ? cell.BoundaryEdge : new IVHalfEdge(cell);
                        currentHalfEdge.SetNext(nextHalfEdge);

                        currentHalfEdge = nextHalfEdge;
                    }
                    
                    Cells.Add(cell);
                }
            }
        }

        private void ConstructStage1(Action<float> UpdateProgress) {

            // Loop over all halfEdges in this chunk
            foreach (var cell in Cells) {

				var halfEdge = cell.BoundaryEdge;
				do {
					
					if (halfEdge.Twin == null) {
						
						// Calculate the expected origin of the cell of the twin edge
						var expectedOrigin = Vector2Ext.Reflect(halfEdge.Origin, halfEdge.Next.Origin - halfEdge.Origin, cell.Origin);

						// Calculate normalChunk and the fractional within the margin of error of the expectedOrigin
						var defaultChunkLocation = new Vector2Int(
							Mathf.FloorToInt(expectedOrigin.x),
							Mathf.FloorToInt(expectedOrigin.y)
						);
						var chunkFractional = expectedOrigin - defaultChunkLocation;

						// Determine in which Chunks the twin cell could also be (due to rounding errors)
						const float errSize = 3f;
                        var config = Plane.Configuration;
						bool couldBePrevX = chunkFractional.x <     config.Epsilon * errSize;
						bool couldBeNextX = chunkFractional.x > 1 - config.Epsilon * errSize;
						bool couldBePrevY = chunkFractional.y <     config.Epsilon * errSize;
						bool couldBeNextY = chunkFractional.y > 1 - config.Epsilon * errSize;

						// Gather all the Chunk(s) of the expected twin cell
						List<Vector2Int> possibleChunkLocations = new List<Vector2Int> { defaultChunkLocation };
						if (couldBePrevX) {
							possibleChunkLocations.Add(new Vector2Int(defaultChunkLocation.x - 1, defaultChunkLocation.y));
						}
						if (couldBeNextX) {
							possibleChunkLocations.Add(new Vector2Int(defaultChunkLocation.x + 1, defaultChunkLocation.y));
						}
						if (couldBePrevY) {
							possibleChunkLocations.Add(new Vector2Int(defaultChunkLocation.x, defaultChunkLocation.y - 1));
							if (couldBePrevX) {
								possibleChunkLocations.Add(new Vector2Int(defaultChunkLocation.x - 1, defaultChunkLocation.y - 1));
							}
							if (couldBeNextX) {
								possibleChunkLocations.Add(new Vector2Int(defaultChunkLocation.x + 1, defaultChunkLocation.y - 1));
							}
						}
						if (couldBeNextY) {
							possibleChunkLocations.Add(new Vector2Int(defaultChunkLocation.x, defaultChunkLocation.y + 1));
							if (couldBePrevX) {
								possibleChunkLocations.Add(new Vector2Int(defaultChunkLocation.x - 1, defaultChunkLocation.y + 1));
							}
							if (couldBeNextX) {
								possibleChunkLocations.Add(new Vector2Int(defaultChunkLocation.x + 1, defaultChunkLocation.y + 1));
							}
						}

						// Search for the twin in each possible chunk
						foreach (var possibleChunkLocation in possibleChunkLocations) {

							// Get the chunk at the possibleChunkLocation
							bool isSelfChunk = possibleChunkLocation == Location;
							var possibleChunk = isSelfChunk ? this : Plane.Get(possibleChunkLocation, -1);

							// If the Chunk is not available, continue to the next one
							if (possibleChunk == null)
								continue;

                            // TODO: Think of a way to adding twins to an neighboring Chunk that is also under construction
                            // For now just skip them
                            while (possibleChunk.Stage < 0) { }

							// Search for a halfEdge of the twin
							var twinHalfEdge = TryFindTwinOf(halfEdge, possibleChunk);

							// If the twinHalfEdge was found, then set it and immediately stop searching
							if (twinHalfEdge != null) {

								halfEdge.SetTwin(twinHalfEdge);
								twinHalfEdge.SetTwin(halfEdge);
								break;

							}


						}

					}

					halfEdge = halfEdge.Next;

				} while (halfEdge != cell.BoundaryEdge);

			}
		}
    }

}
