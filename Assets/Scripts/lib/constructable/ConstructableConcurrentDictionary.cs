﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

public class ConstructableConcurrentDictionary<Key, Value> where Value : Constructable {

    /// <summary>
    /// The Concurrent Dictionary used by this Constructable Concurrent Dictionary as underlying data structure.
    /// </summary>
    private ConcurrentDictionary<Key, Value> _data = new ConcurrentDictionary<Key, Value>();

    /// <summary>
    /// A method that can instantiate the Constructable instances used in this collection. The paramteres are the ConstructableConcurrentDictionary itself, and the key for which to instantiate the instance.
    /// </summary>
    protected Func<ConstructableConcurrentDictionary<Key, Value>, Key, Value> _instantiator;

    /// <summary>
    /// Creates a new Constructable Concurrent Dictionary with the an instantiator function that can instantiate the Constructable instances.
    /// </summary>
    /// <param name="instantiator">A function that can instantiate the Constructable instances. The paramteres are the ConstructableConcurrentDictionary itself, and the key for which to instantiate the instance.</param>
    public ConstructableConcurrentDictionary(Func<ConstructableConcurrentDictionary<Key, Value>, Key, Value> instantiator) {
        _instantiator = instantiator;
    }

    /// <summary>
    /// Get all fully constructed data.
    /// </summary>
    /// <returns>Returns all data that has been fully constructed.</returns>
    public IEnumerable<Value> GetAll() {
        return _data.Values.Where(value => value.IsAtMaxStage);
    }

    /// <summary>
    /// Checks whether the value of a given key is fully constructed.
    /// </summary>
    /// <param name="key">The key</param>
    /// <returns>Returns whether the value of the given key is fully constructed</returns>
    public bool Has(Key key) {
        Value value;
        return _data.TryGetValue(key, out value) && value.IsAtMaxStage;
    }
    
    /// /// <summary>
    /// Checks whether the value of a given key is constructed to at least a given stage.
    /// </summary>
    /// <param name="key">The key</param>
    /// <param name="minimalStage">The minimal stage the value should be</param>
    /// <returns>Returns whether the value of the given key is constructed to at least a given stage</returns>
    internal bool Has(Key key, int minimalStage) {
        Value value;
        return _data.TryGetValue(key, out value) && value.Stage >= minimalStage;
    }

    /// <summary>
    /// Gets the value of the given key as long as it is fully constructed. If the key does not exists, or it has not been fully constructed, null is returned.
    /// </summary>
    /// <param name="key">The key</param>
    /// <returns>Returns the value of the given key as long as it is fully constructed. If the key does not exists, or it has not been fully constructed, null is returned</returns>
    public Value Get(Key key) {
        Value value;
        if (_data.TryGetValue(key, out value) && value.IsAtMaxStage) {
            return value;
        }
        return null;
    }

    /// <summary>
    /// Gets the value of the given key as long as it is constructed to at least a minimal stage. If the key does not exists, or it has not been constructed up to the minimal stage, null is returned.
    /// </summary>
    /// <param name="key">The key</param>
    /// <param name="minimalStage">The minimal stage the value should be</param>
    /// <returns>Returns the value of the given key as long as it is constructed to at least a minimal stage. If the key does not exists, or it has not been constructed up to the minimal stage, null is returned</returns>
    internal Value Get(Key key, int minimalStage) {
        Value value;
        if (_data.TryGetValue(key, out value) && value.Stage >= minimalStage) {
            return value;
        }
        return null;
    }

    /// <summary>
    /// Gets the current construction stage of the given key. If the key does not exists, -1 is returned.
    /// </summary>
    /// <param name="key">The key</param>
    /// <returns>Returns the current construction stage of the given key. If the key does not exists, -1 is returned</returns>
    internal int GetStageOf(Key key) {
        Value value;
        if (_data.TryGetValue(key, out value)) {
            return value.Stage;
        }
        return -1;
    }

    /// <summary>
    /// Request the construction of the value at the given key.
    /// </summary>
    /// <param name="key">The key</param>
    public void Request(Key key) {

        // Check if the Value already exists
        Value value;
        if (_data.TryGetValue(key, out value)) {

            // If it already exists...

            // Request the target stage
            value.RequestTargetStage(value.NumberOfStages - 1);

        } else {

            // If it does not yet exists...

            // Construct a new Value
            value = _instantiator(this, key);

            // Try to add the new Value
            if (_data.TryAdd(key, value)) {

                // Start the construction
                value.RequestTargetStage(value.NumberOfStages - 1);

            } else {

                // Only fails when key is already in the dictionary
                // https://stackoverflow.com/questions/11501931/can-concurrentdictionary-tryadd-fail
                // The chunk must have been requested between trying to get and trying to add in this method (by a different thread)

                // Re-run the request, to ensure the given targetState will be requested
                Request(key);
            }

        }

    }

    /// <summary>
    /// Request the construction of the value at the given key up to the targetStage.
    /// </summary>
    /// <param name="key">The key</param>
    /// <param name="targetStage">The target stage</param>
    internal void Request(Key key, int targetStage) {

        // Check if the Value already exists
        Value value;
        if (_data.TryGetValue(key, out value)) {

            // If it already exists...

            // Request the target stage
            value.RequestTargetStage(targetStage);

        } else {

            // If it does not yet exists...

            // Create a new Value
            value = _instantiator(this, key);

            // Try to add the new Value
            if (_data.TryAdd(key, value)) {

                // Start the construction task
                value.RequestTargetStage(targetStage);

            } else {

                // Only fails when key is already in the dictionary
                // https://stackoverflow.com/questions/11501931/can-concurrentdictionary-tryadd-fail
                // The chunk must have been requested between trying to get and trying to add in this method (by a different thread)

                // Re-run the request, to ensure the given targetState will be requested
                Request(key, targetStage);

            }

        }

    }

}
