﻿using System;
using System.Threading.Tasks;

/// <summary>
/// A thread-safe abstract class describing the process of constructing an object through stages. 
///     The number of stages, 
///     the construction of an stage, and 
///     the deconstruction of an stage 
/// are to be implemented by the class that inherist from Constructable.
/// 
/// The target stage to which the Constructable has to be constructed or deconstructed can be requested. After which execution is directly returned to the caller.
/// The construction and deconstruction of these stages is executed using tasks to allow them to be executed on different threads.
/// </summary>
public abstract class Constructable {

    /// <summary>
    /// Lock used before editing 'TargetStage'.
    /// </summary>
    private object _targetStageLock = new object();
    /// <summary>
    /// Lock used before editing '_isBusy'.
    /// </summary>
    private object _isBusyLock = new object();
    /// <summary>
    /// Boolean to test whether the constructable chunk is currently busy constructing or deconstructing.
    /// </summary>
    public bool IsBusy { get; private set; } = false;


    /// <summary>
    /// The current stage of the ConstructableChunk.
    /// </summary>
    public int Stage { get; private set; } = -1;
    /// <summary>
    /// The target stage of the ConstructableChunk.
    /// </summary>
    public int TargetStage { get; private set; } = -1;
    /// <summary>
    /// The current construction progress as a floating point number between 0 and 1.
    /// </summary>
    public float Progress { get; private set; } = 1f;


    /// <summary>
    /// Whether the current Stage is at the TargetStage
    /// </summary>
    public bool IsAtTargetStage {
        get {
            return Stage == TargetStage;
        }
    }
    /// <summary>
    /// Whether the current Stage is at the MinStage
    /// </summary>
    public bool IsAtMinStage {
        get {
            return Stage == -1;
        }
    }
    /// <summary>
    /// Whether the current Stage is at the MaxStage
    /// </summary>
    public bool IsAtMaxStage {
        get {
            return Stage == NumberOfStages - 1;
        }
    }

    /// <summary>
    /// Request (de-)construction to the given target stage.
    /// </summary>
    /// <param name="_targetStage">The target stage</param>
    public void RequestTargetStage(int _targetStage) {
        bool needsConstruction = false;
        lock (_targetStageLock) {

            // Test whether the given _targetStage is in range of the constructable
            if (_targetStage < -1 || _targetStage > NumberOfStages - 1)
                throw new ArgumentOutOfRangeException("_targetStage");

            // Set the _targetStage only when it is not the current target stage
            if (TargetStage != _targetStage) {
                TargetStage = _targetStage;
                needsConstruction = true;
            }
        }

        // Try to start construction if construction is needed
        if (needsConstruction) {
            TryStartingConstruction();
        }
    }

    /// <summary>
    /// Ensures the construction up to the given target stage.
    /// </summary>
    /// <param name="_targetStage">The target stage</param>
    public void EnsureTargetStage(int _targetStage) {
        bool needsConstruction = false;
        lock (_targetStageLock) {

            // Test whether the given _targetStage is in range of the constructable
            if (_targetStage < -1 || _targetStage > NumberOfStages - 1)
                throw new ArgumentOutOfRangeException("_targetStage");

            // Set the _targetStage only when it is not yet reached
            if (TargetStage < _targetStage) {
                TargetStage = _targetStage;
                needsConstruction = true;
            }
        }

        // Try to start the construction if construction is needed
        if (needsConstruction) {
            TryStartingConstruction();
        }
    }

    /// <summary>
    /// Try to start (de-)constructing the constructable to the target state.
    /// </summary>
    private void TryStartingConstruction() {

        bool isDeconstruction = false;

        lock (_isBusyLock) {

            // Return when construction is already in progress
            if (IsBusy)
                return;

            lock (_targetStageLock) {

                // Return when no construction is needed
                if (IsAtTargetStage) {
                    return;
                }

                // Determine construction or deconstruction within the lock
                isDeconstruction = TargetStage < Stage;

            }

            // Make sure we are the only one constructing this constructable
            IsBusy = true;
        }

        // Create the construction task
        Task task = new Task(() => {

            try {

                // Set the progress on start
                Progress = isDeconstruction ? 1f : 0f;

                // Define the UpdateProgress callback action
                Action<float> UpdateProgress = (float progress) => {
                    Progress = Math.Max(0, Math.Min(1, progress));
                };

                // (De-)Construct a stage
                if (isDeconstruction) {
                    Progress = 1f;
                    Deconstruct(Stage, UpdateProgress);
                    Stage -= 1;
                } else {
                    Progress = 0f;
                    Stage += 1;
                    Construct(Stage, UpdateProgress);
                }

                // Set the progress on end
                Progress = 1f;

            } finally {

                // Finish construction
                lock (_isBusyLock) {
                    IsBusy = false;
                }

            }

            // Try to start construction again to make sure multiple stages can be build consecutively
            TryStartingConstruction();

        });

        // Start the construction task on a different thread
        task.Start();
    }

    /// <summary>
    /// The number of Stages that can be constructed
    /// </summary>
    public abstract int NumberOfStages { get; }

    /// <summary>
    /// The method that constructs a stage. This method is called from within a task and therefor runs on a different thread.
    /// </summary>
    /// <param name="stage">The stage that is constructed from 0 to NumberOfStages - 1</param>
    /// <param name="UpdateProgress">The callback action to which the current progress of the construction between 0 and 1 can be send.</param>
    protected abstract void Construct(int stage, Action<float> UpdateProgress);

    /// <summary>
    /// The method that deconstructs a stage. This method is called from within a task and therefor runs on a different thread.
    /// </summary>
    /// <param name="stage">The stage that is deconstructed from 0 to NumberOfStages - 1</param>
    /// <param name="UpdateProgress">The callback action to which the current progress of the construction between 0 and 1 can be send.</param>
    protected abstract void Deconstruct(int stage, Action<float> UpdateProgress);
}
