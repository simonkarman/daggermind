﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InfiniteVoronoi;

public class DoubleIVPlaneTester : MonoBehaviour {

	private IVPlane masterPlane;
	private IVPlane slavePlane;

	[Header("Visualization")]
    [SerializeField]
    private bool drawAlways = false;
	[SerializeField]
	private float worldScale = 1;

	[Header("Configuration")]
	[SerializeField, ReadOnly(true)]
	private IVConfiguration masterConfiguration;

	[SerializeField, ReadOnly(true)]
	private IVConfiguration slaveConfiguration;

	[SerializeField, ReadOnly(true)]
	private float slaveScale = 0.1f;

	protected void Start() {
		masterPlane = new IVPlane(masterConfiguration);
		slavePlane = new IVPlane(slaveConfiguration);
	}

    protected void Update() {

        var currentLocation = new Vector2(
            transform.localPosition.x / worldScale,
            transform.localPosition.y / worldScale
        );
        var centerChunk = Vector2Int.RoundToInt(currentLocation);
        masterPlane.Request(centerChunk + new Vector2Int( 0,  0));
        masterPlane.Request(centerChunk + new Vector2Int( 0, -1));
        masterPlane.Request(centerChunk + new Vector2Int(-1,  0));
        masterPlane.Request(centerChunk + new Vector2Int(-1, -1));


		var currentLocationSlave = new Vector2(
            transform.localPosition.x / worldScale / slaveScale,
            transform.localPosition.y / worldScale / slaveScale
        );
        var centerChunkSlave = Vector2Int.RoundToInt(currentLocationSlave);
        slavePlane.Request(centerChunkSlave + new Vector2Int( 0,  0));
        slavePlane.Request(centerChunkSlave + new Vector2Int( 0, -1));
        slavePlane.Request(centerChunkSlave + new Vector2Int(-1,  0));
        slavePlane.Request(centerChunkSlave + new Vector2Int(-1, -1));

    }

    protected void OnDestroy() {

        masterPlane = null;
		slavePlane = null;

    }

    public void OnDrawGizmos() {
        if (drawAlways)
            DrawGizmos();
    }

    public void OnDrawGizmosSelected() {
        if (!drawAlways)
            DrawGizmos();
    }

    private void DrawGizmos() {

        if (masterPlane != null)
			DrawIVPlane(masterPlane, new Color(1, 1, 1, 0.4f), 1f);

		if (slavePlane != null)
			DrawIVPlane(slavePlane, new Color(1, 0, 0, 0.4f), slaveScale);
        
	}

	private void DrawIVPlane(IVPlane plane, Color color, float scale) {
        var currentLocation = new Vector2(
            transform.localPosition.x / worldScale / scale,
            transform.localPosition.y / worldScale / scale
        );

        Gizmos.matrix *= Matrix4x4.Scale(Vector3.one * worldScale * scale);
		Gizmos.color = color;

        var currentCell = plane.GetCellAt(currentLocation);

        foreach (var chunk in plane.GetAll()) {

            foreach (var cell in chunk.Cells) {
                
                var edge = cell.BoundaryEdge;
                do {
                    
                    Vector2 a = Vector2.Lerp(edge.Origin, cell.Origin, 0.05f);
                    Vector2 b = Vector2.Lerp(edge.Next.Origin, cell.Origin, 0.05f);

                    if (cell == currentCell) {
						Gizmos.color = Color.cyan;
                    } else {
                        Gizmos.color = color;
                    }
                    
                    Gizmos.DrawLine(a, b);

                    edge = edge.Next;

                } while (edge != cell.BoundaryEdge);
            }
        }
    }

	

}
