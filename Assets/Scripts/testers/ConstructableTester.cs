﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConstructableTester : MonoBehaviour {
    
    private ConstructableTesterMachine ctMachine;

    [Header("Settings")]
    public int stages = 5;
    public int iterations = 15;
    public int msPerIteration = 100;

    [Header("References")]
    public Text nowText;
    public Text targetText;
    public Text numbersText;

    protected void Start() {

        ctMachine = new ConstructableTesterMachine(stages, iterations, msPerIteration);

    }

    protected void Update() {
        
        nowText.text = (ctMachine.Stage + ctMachine.Progress).ToString("0.000");
        targetText.text = (ctMachine.TargetStage).ToString("0");
        numbersText.text = "[" + string.Join(", ", ctMachine.numbers) + "]";

        var currentStage = ctMachine.TargetStage;
        var hasNext = currentStage < ctMachine.NumberOfStages - 1;
        var hasPrev = currentStage > 0;

        if (hasNext && Input.GetKeyDown(KeyCode.UpArrow)) {
            ctMachine.RequestTargetStage(currentStage + 1);
        }

        if (hasPrev && Input.GetKeyDown(KeyCode.DownArrow)) {
            ctMachine.RequestTargetStage(currentStage - 1);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            ctMachine.RequestTargetStage(0);
            System.Threading.Thread.Sleep(11);
            ctMachine.RequestTargetStage(currentStage);
            System.Threading.Thread.Sleep(11);
            ctMachine.RequestTargetStage(ctMachine.NumberOfStages - 1);
        }

    }
    
}


public class ConstructableTesterMachine : Constructable {

    public readonly List<int> numbers = new List<int>();
    public readonly int stages;
    public readonly int iterations;
    public readonly int msPerIteration;

    public ConstructableTesterMachine(int stages, int iterations, int msPerIteration) {
        
        this.stages = stages;
        this.iterations = iterations;
        this.msPerIteration = msPerIteration;

    }

    public override int NumberOfStages => stages;

    protected override void Construct(int stage, Action<float> UpdateProgress) {

        Debug.Log("Constructing " + stage.ToString());

        for (int i = 0; i < iterations; i++) {
            UpdateProgress(i / (float)iterations);
            System.Threading.Thread.Sleep(msPerIteration);
        }

        numbers.Add(stage);

    }

    protected override void Deconstruct(int stage, Action<float> UpdateProgress) {

        Debug.Log("Deconstructing " + stage.ToString());
        
        for (int i = 0; i < iterations; i++) {
            UpdateProgress(1 - (i / (float)iterations));
            System.Threading.Thread.Sleep(msPerIteration);
        }

        numbers.Remove(stage);

    }

}