﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AngleSwapTester : MonoBehaviour {

    public Transform a;
    public Transform b;

    [ContextMenu("Randomize Points")]
    private void RandomizePoints() {
        a.localPosition = new Vector3(Random.value * 5 - 2.5f, 0, Random.value * 5 - 2.5f);
        b.localPosition = new Vector3(Random.value * 5 - 2.5f, 0, Random.value * 5 - 2.5f);
    }

    protected void OnDrawGizmos() {

        if (!(a && b))
            return;

        Vector2 origin2 = new Vector2(transform.position.x, transform.position.z);
        Vector2 a2 = new Vector2(a.position.x, a.position.z);
        Vector2 b2 = new Vector2(b.position.x, b.position.z);

        bool correct = Vector2Ext.IsAngleOrdered(origin2, a2, b2);

        Gizmos.color = correct ? Color.green : Color.red;
        Gizmos.DrawLine(a.position, b.position);
        Gizmos.color = Color.white;
        Gizmos.DrawSphere(transform.position, 0.1f);
        Gizmos.DrawSphere(a.position, 0.1f);
        Gizmos.DrawSphere(b.position, 0.05f);


    }

}
