﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ReflectTester : MonoBehaviour {

    public Transform lineA;
    public Transform lineB;
    public Transform point1;
    public Transform point2;

    protected void Update() {

        Vector2 origin = new Vector2(lineA.position.x, lineA.position.z);
        Vector2 direction = new Vector2(lineB.position.x, lineB.position.z) - origin;
        Vector2 point = new Vector2(point1.position.x, point1.position.z);

        Vector2 mirroredPoint = Vector2Ext.Reflect(origin, direction, point);
        point2.position = new Vector3(mirroredPoint.x, 0, mirroredPoint.y);

    }

    protected void OnDrawGizmos() {

        Gizmos.color = Color.white;
        Gizmos.DrawLine(lineA.position, lineB.position);
        Gizmos.DrawSphere(point1.position, 5f);

        Gizmos.color = Color.gray;
        Gizmos.DrawSphere(point2.position, 5f);

    }


}
