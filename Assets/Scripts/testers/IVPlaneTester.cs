﻿using csDelaunay;
using InfiniteVoronoi;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class IVPlaneTester : MonoBehaviour {

    private IVPlane plane;

    [Header("Visualization")]
    [SerializeField]
    private bool drawAlways = false;
    [SerializeField]
    private float worldScale = 100f;
    [SerializeField]
    private float cellDrawDistance = 8f;
    [SerializeField]
    private Color currentCellEdgeColor = Color.magenta;
    [SerializeField]
    private Color worldBoundaryEdgeColor = Color.red;
    [SerializeField]
    private Color regionBoundaryEdgeColor = Color.green;
    [SerializeField]
    private Color defaultEdgeColor = Color.white;

    [Header("Configuration")]
    [SerializeField, ReadOnly(true)]
    private IVConfiguration config;

    protected void Start() {

        config.ValidateAll();
        config.MarkAsNonEditable();
        plane = new IVPlane(config);
    }

    protected void Update() {

        var currentLocation = new Vector2(
            transform.localPosition.x / worldScale,
            transform.localPosition.y / worldScale
        );
        var centerChunk = Vector2Int.RoundToInt(currentLocation);
        plane.Request(centerChunk + new Vector2Int( 0,  0));
        plane.Request(centerChunk + new Vector2Int( 0, -1));
        plane.Request(centerChunk + new Vector2Int(-1,  0));
        plane.Request(centerChunk + new Vector2Int(-1, -1));

    }

    protected void OnDestroy() {

        plane = null;

    }

    public void OnDrawGizmos() {
        if (drawAlways)
            DrawGizmos();
    }

    public void OnDrawGizmosSelected() {
        if (!drawAlways)
            DrawGizmos();
    }

    private void DrawGizmos() {

        if (plane == null)
            return;
        
        var currentLocation = new Vector2(
            transform.localPosition.x / worldScale,
            transform.localPosition.y / worldScale
        );
        var cellDrawDistanceSqr = cellDrawDistance * cellDrawDistance;

        Gizmos.matrix *= Matrix4x4.Scale(Vector3.one * worldScale);

        var currentCell = plane.GetCellAt(currentLocation);

        foreach (var chunk in plane.GetAll()) {

            foreach (var cell in chunk.Cells) {

                if ((cell.Origin - currentLocation).sqrMagnitude > cellDrawDistanceSqr)
                    continue;
                
                var edge = cell.BoundaryEdge;
                do {
                    
                    Vector2 a = Vector2.Lerp(edge.Origin, cell.Origin, 0.05f);
                    Vector2 b = Vector2.Lerp(edge.Next.Origin, cell.Origin, 0.05f);

                    if (cell == currentCell) {
                        Gizmos.color = currentCellEdgeColor;
                    } else if (edge.Twin == null) {
                        Gizmos.color = worldBoundaryEdgeColor;
                    } else if (edge.Twin.Cell.Chunk != chunk) {
                        Gizmos.color = regionBoundaryEdgeColor;
                    } else {
                        Gizmos.color = defaultEdgeColor;
                    }
                    
                    Gizmos.DrawLine(a, b);

                    edge = edge.Next;

                } while (edge != cell.BoundaryEdge);
            }
        }
    }

}
