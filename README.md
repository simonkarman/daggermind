# Dagger Mind

Infinite World Generation at its finest.

![DaggerMind](Assets/Art/docs/daggermind/daggermind.png)

## Build With

- [Unity3D](http://unity3d.com) - A real-time game engine.
- [Unity-Delaunay](https://github.com/jceipek/Unity-delaunay/) - A C# port for static Voronoi diagram generation.

## Authors

- **Simon Karman** - Initial Work - [Simon Karman](http://www.simonkarman.nl)

## Getting Started

Clone this repository to your filesystem. Download and install `Unity3D` (version `2018.1.17f1`). Open the root directory of the repository as a project in the Unity Editor. Open the scene `Scenes/main.scene`. Press the play button and enjoy!

# World Generation

This project contains a modular setup consisting of multiple libraries that all facilitate the ability to generate infinite worlds. Libraries that were created for, and used by this project are:
- **Constructable** - A library for staged, concurrent, and CPU-intensive construction of objects.
- **InfiniteVoronoi** - A library for seemingly infinite voronoi diagram generation.

## Constructable

A library for staged, concurrent, and CPU-intensive construction of objects.

A `Constructable` is a thread-safe abstract class with an `self-describing` construction process divided into multiple `stages`. A Constructable class provide the number of stages, the construction of each stage, and the deconstruction of each stages. On a Constructable handles `non-blocking` requests to start the `(de-)construction` to any given target stage. The ConstructableConcurrentDictionary facilitates the use of Constructable instances in a concurrent dictionary.

## Infinite Voronoi

A library for seemingly infinite voronoi diagram generation.

A Voronoi diagram is a `partitioning` of the 2D plane into regions based on the distance to points on the plane. This set of points are called the `sites`. Each site has one `region` associated with it. All points within this region lie closer to its site than any other site on the plane. An example of a Voronoi diagram can be found in the picture below.

![Voronoi Diagram Example](Assets/Art/docs/infinitevoronoi.png "Voronoi Diagram Example")

Normally the sites that are used to produce a Voronoi diagram known beforehand, in an infinite world generator this is not the case. To solve this problem, sites can be added and removed `dynamically`. Clever use of this technique makes it possible to construct a seemingly infinite Voronoi diagram. The Voronoi sites are loaded around a `target` point on the plane, when this target moves over the plane, sites too far away from the target are removed, and sites closer to the target are added. The sites are added and removed in batches. One batch of sites is called a chunk.

Two of these Voronoi planes can be `layered` on top of each other. One master layer and one slave layer scaled down significantly. The master layer can then be used to `group` the regions of the slave layer. The resulting grouping is more interesting then the chunked grouping by which the layers are generated. Note that this grouping can ofcourse be `nested` deeper than two levels deep. Some examples:

- *2 layers:* Kingdom to Dukedom
- *3 layers:* Countries to Factions to Tribes
- *4 layers:* Provinces to Districts to Areas to Parcels

